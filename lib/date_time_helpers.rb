module DateTimeHelpers

  module ClassMethods
    def datetime(attribute, opts = {})
      class_eval <<-RUBY_EVAL
        def #{attribute}=(datetime)
          set_datetime :#{attribute}, datetime
        end

        def #{attribute}_for_display
          return #{attribute} unless #{attribute}.is_a?(Time)
          #{attribute}.strftime("%B %-d, %Y at %I:%M%p")
        end
      RUBY_EVAL
    end
  end

  def hours_between(first, second, opts = {})
    opts = { :fractional_zero => false, :round => 2 }.merge(opts)
    hours = ((second - first) / 3600).to_f
    hours = hours.round(opts[:round]) if opts[:round]
    hours = hours.to_i if hours.to_i == hours unless opts[:fractional_zero]
    hours
  end

private

  def validates_chronological_order(*times)
    opts = times.last.is_a?(Hash) ? times.pop : {}
    message = opts[:message] || 'is out of order'
    times.each { |time| validates_datetime(time) }
    times.inject do |previous, current|
      unless errors.on(previous) || errors.on(current)
        ptime, ctime = self.send(previous), self.send(current)
        errors.add(current, message) unless ctime >= ptime
      end
      current
    end
  end

  def validates_datetime(attribute, opts = {})
    message = opts[:message] || 'is not a valid datetime'
    errors.add(attribute, message) unless is_valid_datetime?(self.send(attribute))
  end

  def is_valid_datetime?(datetime)
    result = case datetime
    when nil
      false
    when DateTime, Time
      true
    else
      begin
        DateTime.parse(datetime)
        true
      rescue ArgumentError
        false
      end
    end
  end

  def set_datetime(attribute, datetime)
    self[attribute.to_sym] = datetime
  end
end