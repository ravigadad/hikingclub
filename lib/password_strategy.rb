class PasswordStrategy < Warden::Strategies::Base
  def valid?
    params['username'] || params['password']
  end

  def authenticate!
    u = User.where(:name => params['username']).first
    if u && u.has_password?(params['password'])
      success!(u, "Logged in successfully as #{u.name}!")
    elsif u
      fail!("Wrong password")
    else
      fail!("User not found")
    end
  end
end