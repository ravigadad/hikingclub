FactoryGirl.define do
  to_create { |instance| instance.save }
  factory :club do
    sequence(:name) { |n| "Club ##{n}" }
  end
end