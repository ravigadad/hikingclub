FactoryGirl.define do
  to_create { |instance| instance.save }
  factory :user do
    name 'Arthur Ruhtra'
  end
end