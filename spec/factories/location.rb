FactoryGirl.define do
  to_create { |instance| instance.save }
  factory :location do
    description 'Nowheresville, USA'
  end
end