FactoryGirl.define do
  to_create { |instance| instance.save }
  factory :hike do
    start_time { DateTime.now }
    end_time { start_time + (1/12) }
    title 'Walk on the Moon'
    distance 4.5
    difficulty 'Easy'
    association :start_location, :factory => :location
    club
  end
end