FactoryGirl.define do
  to_create { |instance| instance.save }
  factory :role do
    sequence(:name) { |n| "role_#{n}" }
  end
end