FactoryGirl.define do
  to_create { |instance| instance.save }
  factory :membership do
    user
    role
    club
  end
end