describe User do
  subject { create(:user) }
  let(:club) { create(:club) }
  let(:member_role) { create(:role, :name => 'member') }
  let(:admin_role) { create(:role, :name => 'admin') }

  describe "#can_administer_club?" do
    it 'returns true if user has admin role for club' do
      allow(subject).to receive(:roles_for_club).with(club).and_return([admin_role])
      expect(subject.can_administer_club?(club)).to be_true
    end

    it 'returns false if user is non-admin member of club' do
      allow(subject).to receive(:roles_for_club).with(club).and_return([member_role])
      expect(subject.can_administer_club?(club)).to be_false
    end

    it 'returns false if user is not a member of the club at all' do
      allow(subject).to receive(:roles_for_club).with(club).and_return([])
      expect(subject.can_administer_club?(club)).to be_false
    end
  end

  describe "#password=" do
    it 'sets encrypted password' do
      subject.password = 'satooey'
      expect(BCrypt::Password.new(subject.encrypted_password)).to eq 'satooey'
    end
  end

  describe "#roles_for_club" do
    it 'returns array of roles for given club' do
      other_club = create(:club)
      my_roles = create_list(:role, 2)
      my_roles.each do |r|
        create(:membership, :user => subject, :role => r, :club => club)
      end

      expect(subject.roles_for_club(club)).to eq my_roles
    end
  end
end