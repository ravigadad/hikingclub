module AppHelper
  include Rack::Test::Methods

  def app
    HikingClub::Application
  end
end
