require 'simplecov'
SimpleCov.start

ENV['RORY_STAGE'] = 'test'
require 'factory_girl'
require 'capybara/rspec'

FactoryGirl.find_definitions

require_relative '../config/application'

Dir[Rory.root.join('spec', 'support', '**', '*.rb')].each do |file|
  require file
end

RSpec.configure do |config|
  config.treat_symbols_as_metadata_keys_with_true_values = true
  config.run_all_when_everything_filtered = true
  config.filter_run :focus

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = 'random'
  config.include AppHelper
  config.include FactoryGirl::Syntax::Methods
end

Capybara.app = HikingClub::Application
