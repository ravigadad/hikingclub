ENV['RORY_STAGE'] = 'test'
require 'capybara/cucumber'
require 'webmock/cucumber'
require 'factory_girl'

FactoryGirl.find_definitions

require_relative '../../config/application'

Dir[Rory.root.join('spec', 'support', 'universal', '**', '*.rb')].each do |file|
  require file
end

World(AppHelper)
World(FactoryGirl::Syntax::Methods)

Capybara.app = HikingClub::Application