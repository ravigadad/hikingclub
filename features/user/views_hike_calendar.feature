Feature: User views hike calendar

Scenario: Default
  Given there is a hiking club
  And there are hikes for the club in the past
  And there are hikes for the club in the future
  When I view the hike calendar
  Then I see the future hikes
  And I do not see the past hikes
