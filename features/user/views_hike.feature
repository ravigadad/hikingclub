Feature: User views hike

Scenario: Default
  Given there is a hiking club
  And there is a hike for the club
  When I view the hike
  Then I see the hike details