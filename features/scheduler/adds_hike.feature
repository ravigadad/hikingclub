Feature: Scheduler adds hike

Scenario: Default
  Given there is a hiking club
  And I am a scheduler for the club
  When I add a hike for the club
  And I view the hike
  Then I see the hike details
