Feature: Scheduler edits hike

Scenario: Default
  Given there is a hiking club
  And I am a scheduler for the club
  And there is a hike for the club
  When I edit the hike
  And I view the hike
  Then I see the new hike details
