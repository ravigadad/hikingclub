Given /^there is a hike for the club$/ do
  @hike = create(:hike, :club => @club)
end

Given /^there are hikes for the club in the (past|future)$/ do |time_period|
  @hikes ||= []
  new_hikes = 5.times.collect do |index|
    days_offset = time_period == 'past' ? index * -1 : index
    create(:hike, {
      :title => "#{time_period} hike #{index}",
      :start_time => DateTime.now + (1 * days_offset),
      :club => @club
    })
  end
  @hikes.concat new_hikes
end

When /^I view the hike$/ do
  visit "/clubs/#{@club.id}/hikes/#{@hike.id}"
end

When /^I view the hike calendar$/ do
  visit "/clubs/#{@club.id}/calendar"
end

Then /^I see the (?:new )?hike details$/ do
  expect(page).to have_text(@hike.title)
end

Then(/^I see the future hikes$/) do
  @hikes.each do |hike|
    if hike.start_time > Time.now
      expect(page).to have_text(hike.title)
    end
  end
end

Then(/^I do not see the past hikes$/) do
  @hikes.each do |hike|
    if hike.start_time < Time.now
      expect(page).not_to have_text(hike.title)
    end
  end
end
