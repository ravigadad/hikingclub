Given /^I am a scheduler for the club$/ do
  @user = create(:user)
  @admin_role = create(:role, :name => 'admin')
  @user.add_membership(Membership.create(:role => @admin_role, :club => @club))
  visit "/unauthenticated"
  fill_in :username, :with => @user.name
  click_button :submit
end

When /^I add a hike for the club$/ do
  @leader = create(:user, :name => "Leader McLeaderly")
  @location = create(:location)

  visit "/clubs/#{@club.id}/hikes/new"

  fill_in :hike_title, :with => 'Scarface'
  fill_in :hike_start_time, :with => 'April 5, 2011 at 11:15am'
  fill_in :hike_end_time, :with => 'April 5, 2011 at 2:15pm'
  fill_in :hike_distance, :with => '4.5'
  select 'Moderate', :from => :hike_difficulty
  select @leader.name, :from => :hike_leaders
  select @location.description, :from => :hike_start_location_id

  click_button :submit

  @hike = Hike.last
end

When /^I edit the hike$/ do
  visit "/clubs/#{@club.id}/hikes/#{@hike.id}/edit"

  fill_in :hike_title, :with => 'Some new shenanigans'
end
