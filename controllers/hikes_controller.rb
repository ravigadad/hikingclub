class HikesController < ApplicationController
  before_action :authorize, :except => [:show, :index, :calendar]
  before_action :load_club

  def new
    expose :hike => Hike.new(:club => club)
  end

  def show
    expose :hike => Hike.where(:id => params[:id]).first
  end

  def edit
    expose :hike => Hike.where(:id => params[:id]).first
  end

  def create
    hike = Hike.new(params[:hike])
    hike.save
    redirect "/clubs/#{hike.club_id}/hikes/#{hike.id}"
  rescue Sequel::ValidationFailed
    render 'hikes/new', :locals => locals.merge(:hike => hike)
  end

  def update
    hike = Hike.where(:id => params[:id]).first
    hike.update(params[:hike])
    redirect "/clubs/#{hike.club_id}/hikes/#{hike.id}"
  rescue Sequel::ValidationFailed
    render "hikes/edit", :locals => locals.merge(:hike => hike)
  end

  def index
    expose :hikes => club.hikes
  end

  def calendar
    expose :hikes => club.hikes_dataset.where('start_time >= ?', DateTime.now)
  end

private

  def authorize
    if authenticate
      unless current_user.can_administer_club?(club)
        render_not_found
      end
    end
  end

  def load_club
    if club
      expose :club => club
    else
      render_not_found
    end
  end

  def club
    @club ||= Club.where(:id => params[:club_id]).first
  end
end