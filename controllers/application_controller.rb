class ApplicationController < Rory::Controller
  before_action :expose_current_user

  def layout
    'default'
  end

  def authenticate
    if warden.authenticated?
      true
    else
      session[:back_to] = @request.fullpath
      redirect '/unauthenticated'
      false
    end
  end

private

  def expose_current_user
    expose :current_user => current_user
    expose :auth_message => @request.env["warden"].message
  end

  def current_user
    warden.user
  end

  def warden
    @request.env['warden']
  end

  def session
    @request.env['rack.session']
  end
end