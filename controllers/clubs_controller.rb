class ClubsController < ApplicationController
  def index
    expose :clubs => Club.all
  end

  def show
    redirect "/clubs/#{params[:id]}/hikes"
  end
end