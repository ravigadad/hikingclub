class SessionsController < ApplicationController
  def create
    warden.authenticate!
    redirect session[:back_to] || '/'
  end

  def destroy
    warden.logout
    redirect '/unauthenticated'
  end
end