if ['test', 'development'].include? ENV['RORY_STAGE']
  require 'cucumber/rake/task'
  require 'rspec/core/rake_task'

  Cucumber::Rake::Task.new do |t|
    t.cucumber_opts = %w{--format pretty}
  end

  desc "Run all RSpec specs"
  RSpec::Core::RakeTask.new(:spec)

  task :default => ['db:test:prepare', 'spec', 'cucumber']
end