namespace :hikes do
  task :import => :environment do
    require 'csv'
    hikes = {}
    Dir[Rory.root.join('db', 'csvs', '**', '*.csv')].each do |f|
      CSV.foreach(f, :headers => true).map do |row|
        hike_names = row.headers - ['Last Name', 'First Name', 'Total']
        hike_names.each do |h|
          next unless row[h]
          hikes[h] ||= { :hikers => [], :leaders => [], :writers => [] }

          name = "#{row['First Name'].strip} #{row['Last Name'].strip}"
          u = User.find_or_create(:name => name)

          hikes[h][:hikers] << u if row[h].include?('M')
          hikes[h][:leaders] << u if row[h].include?('L')
          hikes[h][:writers] << u if row[h].include?('W')
        end
      end
    end
    hikes.each_pair do |name, values|
      values[:start_time], values[:end_time] = if name == 'Glacier'
        [Time.new(2013,8,31,10,0,0), Time.new(2013,9,3,10,0,0)]
      else
        convert_hike_name_to_times(name)
      end
      # Hike.find_or_create(:title => name)
    end
    puts hikes.inspect
    # puts hikes.select { |k,v| v[:leaders].length < 1 }.inspect
  end

  def convert_hike_name_to_times(name)
    month, day = name.gsub(/^\w+\s(.*)/, '\1').split('/')
    start_time = Time.new(2013,month,day,10,0,0)
    end_time = start_time + (2 * 3600)
    [start_time, end_time]
  end
end