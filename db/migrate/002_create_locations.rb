Sequel.migration do
  change do
    create_table(:locations) do
      primary_key :id
      String :description, :null => false
      String :directions
      Numeric :latitude
      Numeric :longitude
      Numeric :parking_fee
      Boolean :has_restrooms
      timestamptz :created_at, :null => false
      timestamptz :updated_at, :null => false
    end
  end
end
