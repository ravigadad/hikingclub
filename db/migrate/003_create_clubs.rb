Sequel.migration do
  change do
    create_table(:clubs) do
      primary_key :id
      String :name, :null => false
      String :description
      foreign_key :headquarters_id, :locations
      timestamptz :created_at, :null => false
      timestamptz :updated_at, :null => false
    end
  end
end
