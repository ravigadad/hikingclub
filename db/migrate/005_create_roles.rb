Sequel.migration do
  change do
    create_table(:roles) do
      primary_key :id
      String :name, :unique => true
      String :description
    end

    create_table(:memberships) do
      primary_key :id
      foreign_key :role_id, :roles
      foreign_key :user_id, :users
      foreign_key :club_id, :clubs
    end

    add_index :memberships, [:role_id, :user_id, :club_id], :unique => true
  end
end
