Sequel.migration do
  change do
    create_table(:hikes) do
      primary_key :id
      String :meeting_time
      String :return_time
      timestamptz :start_time, :null => false
      timestamptz :end_time, :null => false
      String :title, :null => false
      String :description
      Numeric :distance, :null => false
      String :difficulty, :null => false
      Numeric :total_elevation_change
      Numeric :lowest_altitude
      Numeric :highest_altitude
      String :trail_conditions
      String :recommended_equipment
      Numeric :carpool_donation
      timestamptz :created_at, :null => false
      timestamptz :updated_at, :null => false
      foreign_key :club_id, :clubs, :null => false
      foreign_key :meeting_location_id, :locations
      foreign_key :start_location_id, :locations, :null => false
      foreign_key :end_location_id, :locations
      foreign_key :posthike_gathering_location_id, :locations
    end

    create_table(:participations) do
      primary_key :id
      foreign_key :hike_id, :hikes, :null => false
      foreign_key :user_id, :users, :null => false
      String :role
    end

    add_index :participations, [:hike_id, :user_id, :role], :unique => true
  end
end
