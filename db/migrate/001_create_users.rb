Sequel.migration do
  change do
    create_table(:users) do
      primary_key :id
      String :name
      String :phone_number
      String :email
      String :encrypted_password, :null => false, :default => ""
      timestamptz :created_at, :null => false
      timestamptz :updated_at, :null => false
    end
  end
end
