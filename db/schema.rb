Sequel.migration do
  change do
    create_table(:locations) do
      primary_key :id
      String :description, :text=>true, :null=>false
      String :directions, :text=>true
      BigDecimal :latitude
      BigDecimal :longitude
      BigDecimal :parking_fee
      TrueClass :has_restrooms
      DateTime :created_at, :null=>false
      DateTime :updated_at, :null=>false
    end
    
    create_table(:roles, :ignore_index_errors=>true) do
      primary_key :id
      String :name, :text=>true
      String :description, :text=>true
      
      index [:name], :name=>:roles_name_key, :unique=>true
    end
    
    create_table(:schema_info) do
      Integer :version, :default=>0, :null=>false
    end
    
    create_table(:users) do
      primary_key :id
      String :name, :text=>true
      String :phone_number, :text=>true
      String :email, :text=>true
      String :encrypted_password, :default=>"", :text=>true, :null=>false
      DateTime :created_at, :null=>false
      DateTime :updated_at, :null=>false
    end
    
    create_table(:clubs) do
      primary_key :id
      String :name, :text=>true, :null=>false
      String :description, :text=>true
      foreign_key :headquarters_id, :locations, :key=>[:id]
      DateTime :created_at, :null=>false
      DateTime :updated_at, :null=>false
    end
    
    create_table(:hikes) do
      primary_key :id
      String :meeting_time, :text=>true
      String :return_time, :text=>true
      DateTime :start_time, :null=>false
      DateTime :end_time, :null=>false
      String :title, :text=>true, :null=>false
      String :description, :text=>true
      BigDecimal :distance, :null=>false
      String :difficulty, :text=>true, :null=>false
      BigDecimal :total_elevation_change
      BigDecimal :lowest_altitude
      BigDecimal :highest_altitude
      String :trail_conditions, :text=>true
      String :recommended_equipment, :text=>true
      BigDecimal :carpool_donation
      DateTime :created_at, :null=>false
      DateTime :updated_at, :null=>false
      foreign_key :club_id, :clubs, :null=>false, :key=>[:id]
      foreign_key :meeting_location_id, :locations, :key=>[:id]
      foreign_key :start_location_id, :locations, :null=>false, :key=>[:id]
      foreign_key :end_location_id, :locations, :key=>[:id]
      foreign_key :posthike_gathering_location_id, :locations, :key=>[:id]
    end
    
    create_table(:memberships, :ignore_index_errors=>true) do
      primary_key :id
      foreign_key :role_id, :roles, :key=>[:id]
      foreign_key :user_id, :users, :key=>[:id]
      foreign_key :club_id, :clubs, :key=>[:id]
      
      index [:role_id, :user_id, :club_id], :unique=>true
    end
    
    create_table(:participations, :ignore_index_errors=>true) do
      primary_key :id
      foreign_key :hike_id, :hikes, :null=>false, :key=>[:id]
      foreign_key :user_id, :users, :null=>false, :key=>[:id]
      String :role, :text=>true
      
      index [:hike_id, :user_id, :role], :unique=>true
    end
  end
end
