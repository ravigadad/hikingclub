Hiking Club
===========

Take a hike!

Install
-------

HikingClub is a [Rory](http://github.com/screamingmuse/rory) app.  It needs to be run via a Rack server (Thin, Unicorn, Puma, etc).
