class Club < Sequel::Model
  one_to_many :hikes, :order => Sequel.desc(:start_time)
  one_to_many :memberships
end