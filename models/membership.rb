class Membership < Sequel::Model
  many_to_one :user
  many_to_one :role
  many_to_one :club
end