require 'pry'
require_relative 'user'
require_relative 'location'
require_relative '../lib/date_time_helpers'

class Hike < Sequel::Model
  include DateTimeHelpers
  extend DateTimeHelpers::ClassMethods

  plugin :forme

  many_to_one :start_location, :class => Location
  many_to_one :club
  one_to_many :participations
  one_to_many :leaderships, :class => :Participation do |ds|
    ds.where(:role => 'leader')
  end
  many_to_many :users, :join_table => :participations
  many_to_many :leaders, :join_table => :participations, :class => User, :right_key => :user_id do |ds|
    ds.where(:participations__role => 'leader')
  end
  nested_attributes :participations, :destroy => true

  datetime :start_time
  datetime :end_time

  Difficulties = %w(Easy Easy+ Moderate Moderate+ Strenuous Strenuous+)

  def validate
    super
    validates_presence [:title, :start_location]
    validates_numeric :distance
    validates_includes Difficulties, :difficulty, :message => 'is not one of the valid choices'
    validates_chronological_order :start_time, :end_time
  end

  def duration
    hours_between(start_time, end_time, :round => 2)
  end

  def leader_pks=(leader_pks)
    self.participations_attributes = leaderships.map { |l| { :id => l.id, :_delete => true } }
    self.participations_attributes = leader_pks.map { |id| { :user_id => id.to_i, :role => 'leader' } }
  end

  def leader_names
    leaderships.map(&:user)
  end
end