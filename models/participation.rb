class Participation < Sequel::Model
  many_to_one :user
  many_to_one :hike
end