require 'bcrypt'

class User < Sequel::Model
  one_to_many :memberships
  many_to_many :clubs, :join_table => :memberships
  one_to_many :participations
  many_to_many :hikes, :join_table => :participations

  def password=(new_password)
    puts new_password
    self.encrypted_password = BCrypt::Password.create(new_password)
  end

  def has_password?(attempted_password)
    BCrypt::Password.new(encrypted_password) == attempted_password
  end

  def to_s
    name
  end

  def can_administer_club?(club)
    roles_for_club(club).any? { |r| r.name == 'admin' }
  end

  def roles_for_club(club)
    memberships_dataset.where(:club => club).map(&:role)
  end

  def participate!(hike, role = 'member')
    add_participation(:hike => hike, :role => role)
  end

  def lead!(hike)
    participate(hike, 'leader')
  end

  def journal!(hike)
    participate(hike, 'journaler')
  end
end