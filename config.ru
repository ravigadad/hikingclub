ENV['RORY_STAGE'] ||= ENV['RACK_ENV'] || 'development'

require File.join(File.dirname(__FILE__), 'config', 'application')

app = Rack::Builder.new {
  map "/assets" do
    run Rack::File.new(Rory.root.join('assets'))
  end

  run HikingClub::Application
}

run app