require 'rack'
require Rory.root.join('lib', 'password_strategy')

Warden::Strategies.add(:password, PasswordStrategy)

HikingClub::Application.use_middleware Rack::Session::Cookie, :secret => "ohmygoshwhatageniussecret"

HikingClub::Application.use_middleware Warden::Manager do |manager|
  manager.default_strategies :password
  manager.failure_app = HikingClub::Application
end
