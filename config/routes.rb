HikingClub::Application.set_routes do
  match '/', :to => 'clubs#index', :methods => [:get]
  match 'clubs', :to => 'clubs#index', :methods => [:get]
  match 'clubs/:id', :to => 'clubs#show', :methods => [:get]
  match 'clubs/:club_id/hikes/new', :to => 'hikes#new', :methods => [:get]
  match 'clubs/:club_id/hikes/:id', :to => 'hikes#show', :methods => [:get]
  match 'clubs/:club_id/hikes/:id/edit', :to => 'hikes#edit', :methods => [:get]
  match 'clubs/:club_id/hikes', :to => 'hikes#create', :methods => [:post]
  match 'clubs/:club_id/hikes/:id', :to => 'hikes#update', :methods => [:put]
  match 'clubs/:club_id/hikes', :to => 'hikes#index', :methods => [:get]
  match 'clubs/:club_id/calendar', :to => 'hikes#calendar', :methods => [:get]
  match 'unauthenticated', :to => 'sessions#new', :methods => [:get, :post]
  match 'authentication/create', :to => 'sessions#create', :methods => [:post]
  match 'authentication/destroy', :to => 'sessions#destroy', :methods => [:get]
end
