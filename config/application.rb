require 'rory'
require 'warden'

module HikingClub
  class Application < Rory::Application
  end
end

HikingClub::Application.root = File.expand_path(File.join('..', '..'), __FILE__)

Dir[HikingClub::Application.root.join('config', 'initializers', '**', '*.rb')].each do |file|
  require file
end

HikingClub::Application.spin_up
HikingClub::Application.require_all_files
